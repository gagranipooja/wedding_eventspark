package com.example.module_1;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;

public class RatesActivity extends Activity {
	Button sb,clr;
	EditText rate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rates);
		rate=(EditText)findViewById(R.id.rates);
		sb=(Button)findViewById(R.id.sub);
		clr=(Button)findViewById(R.id.clr);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.rates, menu);
		return true;
	}

}
