package com.example.module_1;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
//implements OnClickListener 
public class DashboardActivity extends Activity {

	
	Button btn1,btn2,btn3,btn4,btn5;
	private String Deco="Decorator";
	private String Parl="Parlor";
	private String Meha="Mehandi";
	private String Ven="Venue";
	private String Cate="Catering";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dashboard);
		
		btn1=(Button)findViewById(R.id.btndec);
		btn2 = (Button)this.findViewById(R.id.btnparl);
		btn3 = (Button)this.findViewById(R.id.btnmhen);
		btn4= (Button)this.findViewById(R.id.btnven);
		btn5= (Button)this.findViewById(R.id.btncatr);
		
		btn1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("Deco"+Deco);
				 Intent intent2 = new Intent(getApplicationContext(), Dec_Activity.class);
			      intent2.putExtra("typ1", Deco);
			      startActivity(intent2);
				
			}
		});
btn2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("Deco"+Parl);
				 Intent intent2 = new Intent(getApplicationContext(), Dec_Activity.class);
			      intent2.putExtra("typ1", Parl);
			      startActivity(intent2);
				
			}
		});
btn3.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		System.out.println("Deco"+Meha);
		 Intent intent3 = new Intent(getApplicationContext(), MehandiActivity.class);
	      intent3.putExtra("typ1", Meha);
	      startActivity(intent3);
		
	}
});
btn4.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		System.out.println("Deco"+Ven);
		 Intent intent3 = new Intent(getApplicationContext(), Ven_Activity.class);
	      intent3.putExtra("typ1", Ven);
	      startActivity(intent3);
		
	}
});
btn5.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		System.out.println("Deco"+Cate);
		 Intent intent3 = new Intent(getApplicationContext(), Cat_Activity.class);
	      intent3.putExtra("typ1", Cate);
	      startActivity(intent3);
		
	}
});
//		btn2.setOnClickListener(this);
//		btn3.setOnClickListener(this);
//		btn4.setOnClickListener(this);
//		btn5.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dashboard, menu);
		menu.add("logout");
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getTitle().toString().equalsIgnoreCase("logout")) {
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
		}
//		if (item.getTitle().toString().equalsIgnoreCase("Rescan")) {
//			Intent intent = new Intent(this, ScanBooksActivity.class);
//			startActivity(intent);
//		}
		return super.onOptionsItemSelected(item);
	}
	
	
//	public void onClick(View v) {
//		  switch(v.getId()){
////		    case R.id.btndec:
////		      /** Start a new Activity MyCards.java */
////		      Intent intent = new Intent(this, ListActivity.class);
////		      intent.putExtra("typ1", Deco);
////		      this.startActivity(intent);
////		      break;
////		    case R.id.btnparl:
////		      /** AlerDialog when click on Exit */
////		    	Intent i2 = new Intent(this, Dec_Activity.class);
////		    	i2.putExtra("typ1", Parl);
////			      this.startActivity(i2);
////		      break;
//		      
//		    case R.id.btnmhen:
//			      /** AlerDialog when click on Exit */
//			    	Intent i3 = new Intent(this, ListActivity.class);
//			    	i3.putExtra("typ1", Meha);
//				      this.startActivity(i3);
//			      break;
//			      
//		    case R.id.btnven:
//			      /** AlerDialog when click on Exit */
//			    	Intent i4= new Intent(this, ListActivity.class);
//			    	i4.putExtra("typ1", Ven);
//				      this.startActivity(i4);
//			      break;
//		    case R.id.btncatr:
//			      /** AlerDialog when click on Exit */
//			    	Intent i5= new Intent(this, Ven_Activity.class);
//			    	i5.putExtra("typ1", Cate);
//				      this.startActivity(i5);
//			      break;
//			      
//		  }
		//}

}
