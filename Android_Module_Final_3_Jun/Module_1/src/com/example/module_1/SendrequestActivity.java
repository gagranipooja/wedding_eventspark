package com.example.module_1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SendrequestActivity extends Activity implements OnItemSelectedListener {

	EditText et1,et2,et3,et4;
	Spinner spin;
	Button subbtn;
	String reqid,getid;
	TextView amtt;
 static String item,diis;
	String id;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sendrequest);
		et1 = (EditText) findViewById(R.id.etuname);
		et2 = (EditText) findViewById(R.id.etaddr);
		et3 = (EditText) findViewById(R.id.etcont);
		et4 = (EditText) findViewById(R.id.dt);
		subbtn=(Button)findViewById(R.id.btnsub);
		getid=getIntent().getStringExtra("getidto");
		System.out.println(""+getid);
		spin=(Spinner)findViewById(R.id.typspin);
		amtt=(TextView)findViewById(R.id.amt);
		spin.setOnItemSelectedListener(this);

		
		HashMap<String, String> param1 = new HashMap<String, String>();
		param1.put("reqs",item );
		param1.put("ctid", getid);
		String idnew = Network.connect("http://" + Network.IP
				+ "/selectrate", param1);
		System.out.println("isssss"+idnew);
		et1.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (et1.getText().toString().trim().length() < 0) {
					et1.setError(" Field Should Not be Blank. ");
				}
			}
		});

		et2.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (et2.getText().toString().trim().length() < 3) {
					et2
							.setError(" Field Should Not be Blank. OR Less than 3 char ");
				}
			}
		});

		et3.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				if (et1.getText().toString().trim().length() < 0) {
					et1.setError(" Field Should Not be Blank. ");
				}

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (et3.getText().toString().trim().length() < 10
						|| et3.getText().toString().trim()
								.length() > 10) {
					et3
							.setError(" Mobile Number should be 10 Digit.");
				}
			}
		});

		et4.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				if (et4.getText().toString().trim().length() < 0) {
					et4.setError(" Field Should Not be Blank. ");
				}
				
			}
		});
		subbtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				// TODO Auto-generated method stub
				Network.id = id;
				id=LoginActivity.id;
				System.out.println("Network.id "+id);
				//System.out.println("Network.id "+LoginActivity.id);
				if (et1.getText().toString().trim().equalsIgnoreCase("") 
						|| et2.getText().toString().trim().equalsIgnoreCase("")
						|| et3.getText().toString().trim().equalsIgnoreCase("")
						|| et4.getText().toString().trim().equalsIgnoreCase("")
						) {
					Toast.makeText(getApplicationContext(),
							"Please fill all Details", Toast.LENGTH_LONG)
							.show();
				}
				else{		
					HashMap<String, String> param = new HashMap<String, String>();
		//param.put("uidd", Network.id);
		
		param.put("ctid", getid);
			param.put("name", et1.getText().toString().trim());
			param.put("addre",et2.getText().toString().trim());
			param.put("cont",et3.getText().toString().trim());
			param.put("reqs",item );
			param.put("dte",et4.getText().toString().trim() );
			param.put("amt", amtt.getText().toString().trim());

			String id = Network.connect("http://" + Network.IP
					+ "/catering", param);
			id = id.trim();
			System.out.println("id"+id);
		
			
				if (id.equalsIgnoreCase("0")) {
					Toast.makeText(getApplicationContext(),
							"Registeration Unsucessfull...",
							Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(getApplicationContext(), "Registered Successfully",
							Toast.LENGTH_LONG).show();
					Intent intent = new Intent(getApplicationContext(),
							Payment.class);
					startActivity(intent);
				}
			}
//			if (id.equals("0")) {
//				Toast.makeText(getApplicationContext(),
//						" Unsuccessfull ", Toast.LENGTH_LONG)
//						.show();
//			} else if (!id.equals("0")) {
//				Toast.makeText(getApplicationContext(),
//						" Register Successfull ", Toast.LENGTH_LONG).show();
//				Intent intent = new Intent(getApplicationContext(),
//						Payment.class);
//				startActivity(intent);
//				finish();
//			}
				
			}
		//	}
		});
		
		List<String> categories = new ArrayList<String>();
	      categories.add("Party");
	      categories.add("Merraige");
	      categories.add("Reception");
	      
	     ArrayAdapter<String> dat=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,categories);
	     dat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	     
	     spin.setAdapter(dat);
	     

	
}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sendrequest, menu);
		return true;
	}



	@Override
	public void onItemSelected(AdapterView<?> parent, View vww, int position,
			long arg3) {
		// TODO Auto-generated method stub
		 item = parent.getItemAtPosition(position).toString();
		 Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_SHORT).show();

		
	if (item=="Party") {
		
		amtt.setText("2500");
	} else if(item=="Merraige"){
		amtt.setText("10000");
	}else {
		amtt.setText("5000");
	}
		
	}


	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
	private boolean checkNull() {
		// TODO Auto-generated method stub
		boolean isTrue = false;

		if (et1.getText().toString().trim().length() < 0) {
			Toast.makeText(getApplicationContext(),
					" Name Should not be Empty", Toast.LENGTH_LONG).show();
			return isTrue;
		} else if (et2.getText().toString().trim().length() < 0) {
			Toast.makeText(getApplicationContext(),
					" Username Should not be not be Empty",
					Toast.LENGTH_LONG).show();
			return isTrue;
		}  else if (et4.getText().toString().trim().length() <= 0) {
			Toast.makeText(getApplicationContext(),
					" Address Should not be Empty ", Toast.LENGTH_LONG).show();
			return isTrue;
		} else if (et3.getText().toString().trim().length() < 10
				|| et3.getText().toString().trim().length() > 10) {
			Toast.makeText(getApplicationContext(),
					"Mobile number Should be 10 Digit", Toast.LENGTH_LONG)
					.show();
			return isTrue;
		}  else {
			isTrue = true;
			return isTrue;
		}
	}


}
