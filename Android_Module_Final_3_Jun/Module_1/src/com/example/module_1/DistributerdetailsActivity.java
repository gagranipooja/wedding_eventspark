package com.example.module_1;

import java.util.HashMap;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class DistributerdetailsActivity extends Activity {

	String reqid,getid;
	Button about,request;
	String rs;
	TextView tv;
	String disidd;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_distributerdetails);
		disidd=getIntent().getStringExtra("uname");
		about=(Button)findViewById(R.id.btnsub);
		request=(Button)findViewById(R.id.requst);
		about.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent in=new Intent(getApplicationContext(),AboutActivity.class);
				getid=getIntent().getStringExtra("getidfrm");
				System.out.println("getid"+getid);
				in.putExtra("getidto",getid);
				startActivity(in);
			}
		});
		
		
		request.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in2=new Intent(getApplicationContext(),SendrequestActivity.class);
				getid=getIntent().getStringExtra("getidfrm");
				System.out.println("getid"+getid);
				in2.putExtra("getidto",getid);
				startActivity(in2);
				
				
			}
		 
		});
//	 tv=(TextView)findViewById(R.id.tvbout);
//		
//		tv.setText(rs);
//		Intent in=new Intent(DistributerdetailsActivity.this,SendrequestActivity.class);
//		startActivity(in);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.distributerdetails, menu);
		return true;
	}

}
