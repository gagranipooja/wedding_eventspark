package com.example.module_1;

import java.util.HashMap;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegistartionActivity extends Activity implements OnClickListener {

	private EditText et_full_name;
	private EditText et_user_name;
	private EditText et_password;
	private EditText et_address;
	private EditText et_email_address;
	private EditText et_contact_number;
	private Button bt_submit;
	private Button bt_cancal;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registartion);

		et_full_name = (EditText) findViewById(R.id.etname);
		et_user_name = (EditText) findViewById(R.id.et_username);
		et_password = (EditText) findViewById(R.id.etpassword);
		et_address = (EditText) findViewById(R.id.etaddress);
		et_email_address = (EditText) findViewById(R.id.etemail);
		et_contact_number = (EditText) findViewById(R.id.etmobile);
		bt_submit = (Button) findViewById(R.id.btn_register);
		bt_cancal = (Button) findViewById(R.id.btn_cancal);

		bt_submit.setOnClickListener(this);
		bt_cancal.setOnClickListener(this);
		et_full_name.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (et_full_name.getText().toString().trim().length() < 0) {
					et_full_name.setError(" Field Should Not be Blank. ");
				}
			}
		});

		et_password.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (et_password.getText().toString().trim().length() < 3) {
					et_password
							.setError(" Field Should Not be Blank. OR Less than 3 char ");
				}
			}
		});

		et_address.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (et_address.getText().toString().trim().length() <= 0) {
					et_address.setError(" Field Should Not be Blank.");
				}
			}
		});

		et_contact_number.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (et_contact_number.getText().toString().trim().length() < 10
						|| et_contact_number.getText().toString().trim()
								.length() > 10) {
					et_contact_number
							.setError(" Mobile Number should be 10 Digit.");
				}
			}
		});

		et_email_address.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				Is_valid_email_id(et_email_address);
			}
		});

	}

	protected void Is_valid_email_id(EditText email) {
		// TODO Auto-generated method stub
		if (email.getText().toString() == null) {
			email.setError("Invalid Email Address");
			email = null;
		} else if (isEmailValid(email.getText().toString()) == false) {
			email.setError("Invalid Email Address");
			email = null;
		} else {
			email.setError(null);
		}
	}

	private boolean isEmailValid(CharSequence email) {
		// TODO Auto-generated method stub
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registartion, menu);
		return true;
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		switch (view.getId()) {
		case R.id.btn_register:
			boolean istrue = checkNull();
			if (istrue) {
				HashMap<String, String> param = new HashMap<String, String>();
				param.put("name", et_full_name.getText().toString().trim());
				param.put("username", et_user_name.getText().toString().trim());
				param.put("password", et_password.getText().toString().trim());
				param.put("address", et_address.getText().toString().trim());
				param.put("email", et_email_address.getText().toString().trim());
				param.put("mobile", et_contact_number.getText().toString()
						.trim());

				String id = Network.connect("http://" + Network.IP
						+ "/AndroidRegister", param);
				id = id.trim();
				if (id.equals("0")) {
					Toast.makeText(getApplicationContext(),
							" Register Unsuccessfull ", Toast.LENGTH_LONG)
							.show();
				} else if (!id.equals("0")) {
					Toast.makeText(getApplicationContext(),
							" Register Successfull ", Toast.LENGTH_LONG).show();
					Intent intent = new Intent(getApplicationContext(),
							LoginActivity.class);
					startActivity(intent);
					finish();
				}
			}
			break;
		case R.id.btn_cancal:
			Intent intent = new Intent(getApplicationContext(),
					LoginActivity.class);
			startActivity(intent);
			finish();
			break;
		default:
			break;
		}
	}

	private boolean checkNull() {
		// TODO Auto-generated method stub
		boolean isTrue = false;

		if (et_full_name.getText().toString().trim().length() < 0) {
			Toast.makeText(getApplicationContext(),
					" Name Should not be Empty", Toast.LENGTH_LONG).show();
			return isTrue;
		} else if (et_user_name.getText().toString().trim().length() < 3) {
			Toast.makeText(getApplicationContext(),
					" Username Should not be Empty or less tha 3 character",
					Toast.LENGTH_LONG).show();
			return isTrue;
		} else if (et_password.getText().toString().trim().length() < 3) {
			Toast.makeText(getApplicationContext(),
					" password Should not be Empty or less tha 3 character",
					Toast.LENGTH_LONG).show();
			return isTrue;
		} else if (et_address.getText().toString().trim().length() <= 0) {
			Toast.makeText(getApplicationContext(),
					" Address Should not be Empty ", Toast.LENGTH_LONG).show();
			return isTrue;
		} else if (et_contact_number.getText().toString().trim().length() < 10
				|| et_contact_number.getText().toString().trim().length() > 10) {
			Toast.makeText(getApplicationContext(),
					"Mobile number Should be 10 Digit", Toast.LENGTH_LONG)
					.show();
			return isTrue;
		} else if (isEmailValid(et_email_address.getText().toString().trim()) == false) {
			Toast.makeText(getApplicationContext(), " Invalid Email Check.",
					Toast.LENGTH_LONG).show();
			return isTrue;
		} else {
			isTrue = true;
			return isTrue;
		}
	}

}
