package com.example.module_1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import android.R.string;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class DistriRegisterActivity extends Activity implements OnClickListener, OnItemClickListener, OnItemSelectedListener {

	private EditText et_name;
	private EditText et_shop;
	private EditText et_pwd;
	private EditText et_add;
	private EditText et_email;
	private EditText et_cont;
	private Button bt_reg;
	private Button bt_can;
	private Spinner sp;
	 String item;
static String	lecture;
//	 String str="Decorators,Catering,Parlor,Venue";
//	 String[] alphabt={"A","B","C","D"};
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_distri_register);
		et_name=(EditText)findViewById(R.id.etname);
		et_shop=(EditText)findViewById(R.id.shopnae);
		et_pwd=(EditText)findViewById(R.id.etpassword);
		et_add=(EditText)findViewById(R.id.etaddress);
		et_email=(EditText)findViewById(R.id.etemail);
		et_cont=(EditText)findViewById(R.id.etmobile);
		bt_reg=(Button)findViewById(R.id.btn_register);
		bt_can=(Button)findViewById(R.id.btn_cancal);
		Spinner spinner = (Spinner) findViewById(R.id.type);
		//	bt=(Button)findViewById(R.id.getsubbtn);
			 spinner.setOnItemSelectedListener(this);
		
	//	sp=(Spinner)findViewById(R.id.type);
	//	sp.setOnItemClickListener(this);

		bt_reg.setOnClickListener(this);
		bt_can.setOnClickListener(this);
		et_name.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (et_name.getText().toString().trim().length() < 0) {
					et_name.setError(" Field Should Not be Blank. ");
				}
			}
		});

		et_pwd.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (et_pwd.getText().toString().trim().length() < 3) {
					et_pwd
							.setError(" Field Should Not be Blank. OR Less than 3 char ");
				}
			}
		});

		et_add.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (et_add.getText().toString().trim().length() <= 0) {
					et_add.setError(" Field Should Not be Blank.");
				}
			}
		});

		et_cont.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (et_cont.getText().toString().trim().length() < 10
						|| et_cont.getText().toString().trim()
								.length() > 10) {
					et_cont
							.setError(" Mobile Number should be 10 Digit.");
				}
			}
		});

		et_email.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				Is_valid_email_id(et_email);
			}
		});
	
		  List<String> categories = new ArrayList<String>();
	      categories.add("Decorator");
	      categories.add("Parlor");
	      categories.add("Mehandi");
	      categories.add("Venue");
	      categories.add("Catering");
	      //positioncategories.add("Travel");
	      
//	      // Creating adapter for spinner
//	      ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
//	      
//	      // Drop down layout style - list view with radio button
//	      dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//	      
//	      // attaching data adapter to spinner
//	      sp.setAdapter(dataAdapter);
	      ArrayAdapter<String> adp=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,categories);
	      adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	      spinner.setAdapter(adp);
	      
	
	
}
//	@Override
//	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//		// TODO Auto-generated method stub
//	 item = parent.getItemAtPosition(position).toString();
//	      
//	      // Showing selected spinner item
//	      Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
//	   }
	protected void Is_valid_email_id(EditText email) {
		// TODO Auto-generated method stub
		if (email.getText().toString() == null) {
			email.setError("Invalid Email Address");
			email = null;
		} else if (isEmailValid(email.getText().toString()) == false) {
			email.setError("Invalid Email Address");
			email = null;
		} else {
			email.setError(null);
		}
	}

	private boolean isEmailValid(CharSequence email) {
		// TODO Auto-generated method stub
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_register:
			boolean istrue = checkNull();
			if (istrue) {
				HashMap<String, String> param = new HashMap<String, String>();
				param.put("name", et_name.getText().toString().trim());
				param.put("shopnam", et_shop.getText().toString().trim());
				param.put("password", et_pwd.getText().toString().trim());
				param.put("address", et_add.getText().toString().trim());
				param.put("email", et_email.getText().toString().trim());
				param.put("mobile", et_cont.getText().toString()
						.trim());
				param.put("typee",item);

				String id = Network.connect("http://" + Network.IP
						+ "/distributerregis", param);
				id = id.trim();
				if (id.equals("0")) {
					Toast.makeText(getApplicationContext(),
							" Register Unsuccessfull ", Toast.LENGTH_LONG)
							.show();
				} else if (!id.equals("0")) {
					Toast.makeText(getApplicationContext(),
							" Register Successfull ", Toast.LENGTH_LONG).show();
					Intent intent = new Intent(getApplicationContext(),
							MainDashbrdActivity.class);
					startActivity(intent);
					finish();
				}
			}
			break;
		case R.id.btn_cancal:
			Intent intent = new Intent(getApplicationContext(),
					LoginActivity.class);
			startActivity(intent);
			finish();
			break;
		default:
			break;
		}
		
	}
	private boolean checkNull() {
		// TODO Auto-generated method stub
		boolean isTrue = false;

		if (et_name.getText().toString().trim().length() < 0) {
			Toast.makeText(getApplicationContext(),
					" Name Should not be Empty", Toast.LENGTH_LONG).show();
			return isTrue;
		} else if (et_shop.getText().toString().trim().length() < 3) {
			Toast.makeText(getApplicationContext(),
					" Username Should not be Empty or less tha 3 character",
					Toast.LENGTH_LONG).show();
			return isTrue;
		} else if (et_pwd.getText().toString().trim().length() < 3) {
			Toast.makeText(getApplicationContext(),
					" password Should not be Empty or less tha 3 character",
					Toast.LENGTH_LONG).show();
			return isTrue;
		} else if (et_add.getText().toString().trim().length() <= 0) {
			Toast.makeText(getApplicationContext(),
					" Address Should not be Empty ", Toast.LENGTH_LONG).show();
			return isTrue;
		} else if (et_cont.getText().toString().trim().length() < 10
				|| et_cont.getText().toString().trim().length() > 10) {
			Toast.makeText(getApplicationContext(),
					"Mobile number Should be 10 Digit", Toast.LENGTH_LONG)
					.show();
			return isTrue;
		} else if (isEmailValid(et_email.getText().toString().trim()) == false) {
			Toast.makeText(getApplicationContext(), " Invalid Email Check.",
					Toast.LENGTH_LONG).show();
			return isTrue;
		} else {
			isTrue = true;
			return isTrue;
		}
	}
	@Override
	public void onItemSelected(AdapterView<?> parent, View vw, int position,
			long arg3) {
		// TODO Auto-generated method stub
		 item = parent.getItemAtPosition(position).toString();
	      
	      // Showing selected spinner item
	      Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
		
	}
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}


	
		
	



	
	

}
