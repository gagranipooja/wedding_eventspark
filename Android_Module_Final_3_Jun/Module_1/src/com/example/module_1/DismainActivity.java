package com.example.module_1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class DismainActivity extends Activity implements OnClickListener, OnItemSelectedListener {
	private EditText et_name;
	private EditText et_password;
	private Button bt_register;
	private Button bt_login;
	public static String idd;
	Spinner spn;
	String item;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dismain);
		et_name = (EditText) findViewById(R.id.username);
		et_password = (EditText) findViewById(R.id.password);
		bt_login = (Button) findViewById(R.id.btnlog);
		bt_register = (Button) findViewById(R.id.btnreg);
		spn=(Spinner)findViewById(R.id.spintpe);
		spn.setOnItemSelectedListener(this);
		bt_login.setOnClickListener(this);
		bt_register.setOnClickListener(this);
		List<String> categories = new ArrayList<String>();
	      categories.add("Decorator");
	      categories.add("Parlor");
	      categories.add("Mehandi");
	      categories.add("Venue");
	      categories.add("Catering");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
	      dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	      
	      // attaching data adapter to spinner
	      spn.setAdapter(dataAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dismain, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		switch (v.getId()) {
		case R.id.btnlog:
			if (et_name.getText().toString().trim().length() <= 0
					|| et_password.getText().toString().trim().length() <= 0) {
				Toast.makeText(getApplicationContext(),
						" All Field are compulsory ", Toast.LENGTH_LONG).show();
			} else {
				HashMap<String, String> param = new HashMap<String, String>();
				param.put("username", et_name.getText().toString().trim());
				param.put("password", et_password.getText().toString().trim());
				param.put("tp", item);
				String id = Network.connect("http://" + Network.IP
						+ "/distributorlogin", param);
				id = id.trim();
				if (id.equals("0")) {
					Toast.makeText(getApplicationContext(), " Login Failed,You have to Register first ",
							Toast.LENGTH_LONG).show();
				} else if (!id.equals("0")) {
					Intent intent = new Intent(getApplicationContext(),
							DisaccActivity.class);
					finish();
					Network.id = id;
					intent.putExtra("Networkid", Network.id);
					startActivity(intent);
					System.out.println("uid"+Network.id);
					finish();
				}
			}
			break;
		case R.id.btnreg:
			Intent intent = new Intent(getApplicationContext(),
					DistriRegisterActivity.class);
			startActivity(intent);
			finish();
			break;
		default:
			break;
		
	}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View vw, int position,
			long arg3) {
		// TODO Auto-generated method stub
		 item = parent.getItemAtPosition(position).toString();
	      
	      // Showing selected spinner item
	      Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_SHORT).show();
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}

}
