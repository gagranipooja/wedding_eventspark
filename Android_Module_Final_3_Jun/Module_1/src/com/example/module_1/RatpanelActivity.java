package com.example.module_1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class RatpanelActivity extends Activity implements OnItemSelectedListener {

	Spinner spn;
	String item;
	Button adbtn;
	EditText etamt;
	String idd;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ratpanel);
		spn=(Spinner)findViewById(R.id.spni);
		adbtn=(Button)findViewById(R.id.add);
		 etamt=(EditText)findViewById(R.id.Etamt);
		 idd=getIntent().getStringExtra("Network");
			System.out.println("idd"+idd);
		spn.setOnItemSelectedListener(this);
		List<String> categories = new ArrayList<String>();
		categories.add("Party");
	      categories.add("Merraige");
	      categories.add("Reception");
	      
//	      param.put("typ", idd);
	     // etamt=(EditText)findViewById(R.id.Etamt);
	      ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
	      dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	      spn.setAdapter(dataAdapter);
	      adbtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				HashMap<String, String> param = new HashMap<String, String>();

			      param.put("typ", idd);
				param.put("ratee", etamt.getText().toString().trim());
				param.put("ty", item);
				String id = Network.connect("http://" + Network.IP
						+ "/addrates", param);
				id = id.trim();
				if (id.equals("0")) {
					Toast.makeText(getApplicationContext(),
							" Register Unsuccessfull ", Toast.LENGTH_LONG)
							.show();
				} else if (!id.equals("0")) {
					Toast.makeText(getApplicationContext(),
							" Register Successfull ", Toast.LENGTH_LONG).show();
					Intent intent = new Intent(getApplicationContext(),
							MainDashbrdActivity.class);
					startActivity(intent);
					finish();
				}
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ratpanel, menu);
		return true;
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View vww, int position,
			long arg3) {
		// TODO Auto-generated method stub
		 item = parent.getItemAtPosition(position).toString();
	      
	      // Showing selected spinner item
	      Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}

}
