package com.example.module_1;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainDashbrdActivity extends Activity {

	Button dis_btn,usr_btn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_dashbrd);
		
		dis_btn=(Button)findViewById(R.id.distrbtn);
		usr_btn=(Button)findViewById(R.id.usrbtn);
		
		dis_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in1=new Intent(getApplicationContext(),LoginActivity.class);
				startActivity(in1);
			}
		});
		
		usr_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in1=new Intent(getApplicationContext(),DismainActivity.class);
				startActivity(in1);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_dashbrd, menu);
		return true;
	}

}
