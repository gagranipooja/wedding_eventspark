package com.example.module_1;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class FinalpayActivity extends Activity implements OnCheckedChangeListener {
	private CheckBox checkBox1, checkBox2, checkBox3;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_finalpay);
		
		checkBox1 = (CheckBox) findViewById(R.id.crdchk);
		checkBox2 = (CheckBox) findViewById(R.id.dbtchk);
		
		checkBox1.setOnCheckedChangeListener(this);
	checkBox2.setOnCheckedChangeListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.finalpay, menu);
		return true;
	}

	@Override
	public void onCheckedChanged(CompoundButton btnview, boolean ischk) {
		// TODO Auto-generated method stub
switch (btnview.getId()) {
case R.id.crdchk:
	Intent intent= new Intent(getApplicationContext(),Credit_pay.class);
	startActivity(intent);
	break;
case R.id.dbtchk:
	Intent intent1= new Intent(getApplicationContext(),Debit_pay.class);
	startActivity(intent1);
	
	break;

default:
	break;
}
		
	}

}
