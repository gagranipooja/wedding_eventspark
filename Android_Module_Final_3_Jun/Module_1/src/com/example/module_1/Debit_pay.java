package com.example.module_1;

import java.util.HashMap;

import org.apache.http.protocol.HTTP;



import android.R.string;
import android.net.ParseException;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.content.Intent;

public class Debit_pay extends Activity {
	String customer_name,racename;
	EditText cvv,crd,date;
	Button pay;
	Spinner spinner1, spinner2;
	
	//MainActivity userFunction = new MainActivity();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final Intent intent = getIntent();
		
		setContentView(R.layout.activity_debit_pay);
		//addListenerOnSpinnerItemSelection();
		cvv = (EditText) findViewById(R.id.dbtcvv);
		crd = (EditText) findViewById(R.id.dbtno);
		pay=(Button)findViewById(R.id.dbtpay);
		
		crd.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if(crd.getText().toString().matches("^0")) {
					Toast.makeText(getApplicationContext(),
							"First digit should be non-zero", Toast.LENGTH_LONG)
							.show();
					crd.setText("");
				} 

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		cvv.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if(cvv.getText().toString().matches("^0")) {
					Toast.makeText(getApplicationContext(),
							"First digit should be non-zero", Toast.LENGTH_LONG)
							.show();
					cvv.setText("");
				} 

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		pay.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				String month_number = Storage.month.get(spinner1
//						.getSelectedItem().toString().trim());
//				String year_number = Storage.year.get(spinner2.getSelectedItem()
//						.toString().trim());
//				
				// TODO Auto-generated method stub
				String crno=crd.getText().toString();
				String cvvno=cvv.getText().toString();
				 
				if(crno.equalsIgnoreCase("")||cvvno.equalsIgnoreCase(""))
				{
					
					Toast.makeText(getApplicationContext(),
							"Please fill all field", Toast.LENGTH_SHORT).show();
				
				}
				if(crd.getText().toString().matches("^0")) {
					Toast.makeText(getApplicationContext(),
							"First digit should be non-zero", Toast.LENGTH_LONG)
							.show();
					crd.setText("");
				} 
				 if(crno.length()!=16)
				{
					
					crd.setError("only 16 digit");
				}else 
				if(cvvno.length()<3||cvvno.length()>3)
				{
					
					cvv.setError("only 3 digit");
				}
				else if(cvv.getText().toString().matches("^0")) {
					Toast.makeText(getApplicationContext(),
							"First digit should be non-zero", Toast.LENGTH_LONG)
							.show();
					cvv.setText("");
				} 
				else
				{
//					HashMap<String, String> param1 = new HashMap<String, String>();
//					
//					racename = intent.getStringExtra("racename");
//					param1.put("username",LoginActivity.uname);
//					param1.put("racename" , racename);
//					System.out.println("racename : " + racename);
//					param1.put("horsename",intent.getStringExtra("horsename"));
//					param1.put("amount", intent.getStringExtra("amount"));
//					param1.put("crd", crno);
//					
//						String rs1 = Network.connect("http://" + Network.IP + "/Betting", param1);
//						rs1=rs1.trim();
//						
//						if (rs1.equals("0")) {
//
//							Toast.makeText(getApplicationContext(),
//									" betting unsuccessfull ", Toast.LENGTH_LONG).show();
//						} else if (!rs1.equals("0")) {	   
//					   
//					   
//							
//						}
						
						
					Toast.makeText(getApplicationContext(),
							" Your Payment done successfull'", Toast.LENGTH_LONG).show();
					Intent it=new Intent(getApplicationContext(),DashboardActivity.class);
					startActivity(it);
				}
				
				
				
			}
			
			
			
		});
		
		
	}
	
//	private void addListenerOnSpinnerItemSelection() {
//		// TODO Auto-generated method stub
//		spinner1 = (Spinner) findViewById(R.id.spinnerdb1);
//		spinner1.setOnItemSelectedListener(new CustomOnItemSelectedListener());
//		spinner2 = (Spinner) findViewById(R.id.spinnerdb2);
//		spinner2.setOnItemSelectedListener(new CustomOnItemSelectedListener());
//	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.credit_pay, menu);
		return true;
	}

}
