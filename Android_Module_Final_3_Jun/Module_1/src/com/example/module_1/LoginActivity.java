package com.example.module_1;

import java.util.HashMap;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity implements OnClickListener {
	private EditText et_name;
	private EditText et_password;
	private Button bt_register;
	private Button bt_login;
	public static String id=Network.id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		et_name = (EditText) findViewById(R.id.username);
		et_password = (EditText) findViewById(R.id.password);
		bt_login = (Button) findViewById(R.id.btnlog);
		bt_register = (Button) findViewById(R.id.btnreg);

		bt_login.setOnClickListener(this);
		bt_register.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		switch (view.getId()) {
		case R.id.btnlog:
			if (et_name.getText().toString().trim().length() <= 0
					|| et_password.getText().toString().trim().length() <= 0) {
				Toast.makeText(getApplicationContext(),
						" All Field are compulsory ", Toast.LENGTH_LONG).show();
			} else {
				HashMap<String, String> param = new HashMap<String, String>();
				param.put("username", et_name.getText().toString().trim());
				param.put("password", et_password.getText().toString().trim());
				String id = Network.connect("http://" + Network.IP
						+ "/AndroidLogin", param);
				id = id.trim();
				if (id.equals("0")) {
					Toast.makeText(getApplicationContext(), " Login Failed,You have to Register first ",
							Toast.LENGTH_LONG).show();
				} else if (!id.equals("0")) {
					Intent intent = new Intent(getApplicationContext(),
							DashboardActivity.class);
					finish();
					Network.id = id;
					
					System.out.println("uid"+Network.id);
					startActivity(intent);
					finish();
				}
			}
			break;
		case R.id.btnreg:
			Intent intent = new Intent(getApplicationContext(),
					RegistartionActivity.class);
			startActivity(intent);
			finish();
			break;
		default:
			break;
		}
	}

}
