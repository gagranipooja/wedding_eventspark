-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 03, 2016 at 04:41 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `module_1`
--

-- --------------------------------------------------------

--
-- Table structure for table `all _data`
--

CREATE TABLE `all _data` (
  `id` int(11) NOT NULL,
  `dis_id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `addres` varchar(50) NOT NULL,
  `cont` varchar(50) NOT NULL,
  `reqs` varchar(50) NOT NULL,
  `amt` int(20) NOT NULL,
  `date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `all _data`
--

INSERT INTO `all _data` (`id`, `dis_id`, `name`, `addres`, `cont`, `reqs`, `amt`, `date`) VALUES
(1, '1', 'sid', 'nashik', '9876543210', 'party', 1000, ''),
(2, '3', 'Mahesh', 'Nashik', '1234567890', 'Party', 2500, ''),
(3, '1', 'Manoj', 'Nashik', '9876543201', 'Reception', 5000, ''),
(4, '1', 'Man', 'Nashik', '1134567890', 'Party', 2500, '2007/2/2'),
(5, '1', 'Msn', 'Nashik', '9876543210', 'Party', 2500, '2'),
(6, '1', 'Mk', 'Nashik', '1234567890', 'Party', 2500, '12/12/2015'),
(7, '1', 'Mk', 'Nashik', '1234567890', 'Party', 2500, '12/12/2'),
(8, '1', 'Mahesh', 'Nashik', '9087654321', 'Party', 2500, '1/2/2015'),
(9, '1', 'Man', 'Nashik', '9087654321', 'Party', 2500, '2/12/2014'),
(10, '1', 'Sidd', 'Nashik', '9087654321', 'Party', 2500, '2/2/2014'),
(11, '1', 'Sid', 'Nashik', '9087654331', 'Party', 2500, '2/2/2016'),
(12, '1', 'Siddharth', 'Nashik', '1234567890', 'Party', 2500, '2/2/2916'),
(13, '1', 'Man', 'Nashik', '9087654321', 'Party', 2500, '2/2/2012'),
(14, '1', 'Manoj', 'Nashik', '9012346678', 'Party', 2500, '2/2/2016'),
(15, '1', 'Nikita', 'Nashik', '1234567890', 'Party', 2500, '2/2/2013'),
(16, '1', 'Dev', 'Nashik', '9087654321', 'Party', 2500, '2/2/2016'),
(17, '2', 'Ganesh', 'Nashik', '2345678901', 'Party', 2500, '2/2/2016'),
(18, '3', 'Ramji', 'Nashik', '1234567890', 'Party', 2500, '22/1/2016'),
(19, '3', 'Ramjane', 'Nashik', '2345678901', 'Party', 2500, '2/2/2016'),
(20, '1', 'Sid', 'nashik', '123456789', 'Merraige', 10000, '12/12/2016'),
(21, '6', '', '', '', 'Party', 2500, ''),
(22, '6', '', '', '', 'Party', 2500, ''),
(23, '1', '', '', '', 'Party', 2500, ''),
(24, '1', '', '', '', 'Party', 2500, ''),
(25, '1', '', '', '', 'Party', 2500, ''),
(26, '1', 'Siddharth', 'Nashik', '8934567890', 'Party', 2500, '1/2/2012');

-- --------------------------------------------------------

--
-- Table structure for table `cate`
--

CREATE TABLE `cate` (
  `id` int(11) NOT NULL,
  `catid` int(50) NOT NULL,
  `uname` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `contact` varchar(12) NOT NULL,
  `requestfor` varchar(100) NOT NULL,
  `amount` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cate`
--

INSERT INTO `cate` (`id`, `catid`, `uname`, `address`, `contact`, `requestfor`, `amount`) VALUES
(1, 4, 'anku', 'panvel', '9004118193', 'party', 1000),
(2, 3, 'Mahesh', 'Nashik', '113456789', 'party', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `decr`
--

CREATE TABLE `decr` (
  `id` int(11) NOT NULL,
  `uid` int(50) NOT NULL,
  `decid` int(50) NOT NULL,
  `uname` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `contact` int(10) NOT NULL,
  `requestfor` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `distributer`
--

CREATE TABLE `distributer` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(50) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `distributer`
--

INSERT INTO `distributer` (`id`, `name`, `username`, `password`, `email`, `address`, `mobile`, `type`) VALUES
(1, 'Patil and family', 'Sidd99', '123456', 'Sidd@gmail.com', 'Nashik', '9578123456', 'Decorator'),
(2, 'Samarth', 'Sam', '123456', 'Samart@gmail.com', 'Nashuk', '9876543210', 'Parlor'),
(3, 'Radhakrishna', 'Ram', '123456', 'Ram@gmail.com', 'Nashik', '9078564321', 'Decorator'),
(4, 'Krishna catering', 'Krish', '123456', 'Krish@gmail.com', 'Nashik', '9087654321', 'Catering'),
(5, 'Madhuri mehandi', 'Madhu', '123456', 'Madhu@gmail.com', 'Nashik', '9087654312', 'Mehandi'),
(6, 'Radhakrishn ven', 'Radh', '123456', 'Radh@gmail.com', 'Nashik', '9876543290', 'Venue'),
(7, 'Dipalee', 'Dips', 'dips1234', 'dipaleesangpal@gmail.com', 'Pimpalgaon', '9028268779', 'Mehandi');

-- --------------------------------------------------------

--
-- Table structure for table `mehan`
--

CREATE TABLE `mehan` (
  `id` int(11) NOT NULL,
  `uid` int(50) NOT NULL,
  `mehrid` int(50) NOT NULL,
  `uname` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `contact` int(10) NOT NULL,
  `requestfor` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `parl`
--

CREATE TABLE `parl` (
  `id` int(11) NOT NULL,
  `uid` int(50) NOT NULL,
  `prid` int(50) NOT NULL,
  `uname` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `contact` int(10) NOT NULL,
  `requestfor` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

CREATE TABLE `rates` (
  `id` int(11) NOT NULL,
  `disid` int(50) NOT NULL,
  `amt` int(100) NOT NULL,
  `typee` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rates`
--

INSERT INTO `rates` (`id`, `disid`, `amt`, `typee`) VALUES
(1, 1, 1000, 'Decorator'),
(2, 1, 1000, 'Decorator'),
(3, 1, 9000, 'Merraige'),
(4, 1, 1000, 'Party'),
(5, 1, 4456488, 'Party');

-- --------------------------------------------------------

--
-- Table structure for table `requestlist`
--

CREATE TABLE `requestlist` (
  `id` int(11) NOT NULL,
  `userid` int(50) NOT NULL,
  `runame` varchar(100) NOT NULL,
  `disid` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `mobile` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `email`, `address`, `mobile`) VALUES
(1, '', 'Sid', '123456', 'Siddharth.eiosys@gmail.com', 'Nashik', 9404688826),
(2, '', 'Chetan', '123456', 'Sid@gmail.com', 'Nashik', 9402638798),
(3, '', 'Manoj', '123456', 'Man@gmail.com', 'Nashik', 9226381998),
(4, 'Deepak', 'Dss', 'Qwerty', 'deepaksirola@gmail.com', 'Nashik', 8769588997),
(5, 'Divu', 'Dda', '1234', 'Hhdjjk@gmail.com', 'Ddidjj', 7282999912);

-- --------------------------------------------------------

--
-- Table structure for table `venu`
--

CREATE TABLE `venu` (
  `id` int(11) NOT NULL,
  `uid` int(50) NOT NULL,
  `prid` int(50) NOT NULL,
  `uname` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `contact` int(10) NOT NULL,
  `requestfor` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `all _data`
--
ALTER TABLE `all _data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cate`
--
ALTER TABLE `cate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `decr`
--
ALTER TABLE `decr`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `distributer`
--
ALTER TABLE `distributer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mehan`
--
ALTER TABLE `mehan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parl`
--
ALTER TABLE `parl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rates`
--
ALTER TABLE `rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requestlist`
--
ALTER TABLE `requestlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `name` (`email`,`mobile`);

--
-- Indexes for table `venu`
--
ALTER TABLE `venu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `all _data`
--
ALTER TABLE `all _data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `cate`
--
ALTER TABLE `cate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `decr`
--
ALTER TABLE `decr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `distributer`
--
ALTER TABLE `distributer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `mehan`
--
ALTER TABLE `mehan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `parl`
--
ALTER TABLE `parl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rates`
--
ALTER TABLE `rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `requestlist`
--
ALTER TABLE `requestlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `venu`
--
ALTER TABLE `venu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
