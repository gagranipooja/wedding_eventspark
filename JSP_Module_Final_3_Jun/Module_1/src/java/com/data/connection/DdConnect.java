package com.data.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DdConnect {

    static Connection connection = null;
    static Statement statement = null;
    static String url = "jdbc:mysql://localhost:3306/";
    static String dbName = "module_1";
    static String driver = "com.mysql.jdbc.Driver";
    static String userName = "root";
    static String password = "";

    public static Statement connect() throws Exception {

        try {
            Class.forName(driver).newInstance();
            connection = DriverManager.getConnection(url + dbName, userName,
                    password);
            statement = connection.createStatement();
        } catch (Exception e) {
        }
        return statement;
    }

    public static Connection connect1() throws Exception {
        try {
            Class.forName(driver).newInstance();
            connection = DriverManager.getConnection(url + dbName, userName,
                    password);
        } catch (Exception e) {
        }
        return connection;
    }

    public static void close() {
        try {
            connection.close();
            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(DdConnect.class.getName()).log(Level.SEVERE, null,
                    ex);
        }
    }

    public static long getInsertId() {
        try {
            Object[] insertIds = getInsertIds();
            return Long.parseLong("" + insertIds[0]);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static Object[] getInsertIds() {
        try {
            if (statement != null) {
                DdConnect.statement = statement;
            } else {
                statement = DdConnect.statement;
            }
            ResultSet generatedKeys = statement.getGeneratedKeys();
            ArrayList<Long> keys = new ArrayList<Long>();
            while (generatedKeys.next()) {
                keys.add(generatedKeys.getLong(1));
            }
            return keys.toArray();
        } catch (SQLException e) {
            System.out.println(e.getLocalizedMessage());
            return null;
        }
    }

}
