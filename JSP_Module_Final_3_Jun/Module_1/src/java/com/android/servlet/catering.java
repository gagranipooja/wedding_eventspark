/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.android.servlet;

import com.data.connection.DdConnect;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author eiosys
 */
public class catering extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            Statement statement;
            int isInserted = 0;
            //String uidd1 = request.getParameter("uidd");
            String ctid1 = request.getParameter("ctid");
            String username = request.getParameter("name");
            String adr = request.getParameter("addre");
            String cnt = request.getParameter("cont");
            String req=request.getParameter("reqs");
            String am=request.getParameter("amt");
             String dtee=request.getParameter("dte");
//rates//            String email = request.getParameter("email");
//            String address = request.getParameter("address");
//           String mobile = request.getParameter("mobile");

//            String sql = "INSERT INTO `cate` "
//                    + "(`id`,`catid`, `uname`,`address`, `contact`,`requestfor`,`amount`) "
//                    + "VALUES (NULL,'" + ctid1 + "', '"
//                    + username + "','" + adr + "','" + cnt + "','" + req
//                    + "','" + am
//                    + "');";
            
            String sql= "INSERT INTO `all _data`(`id`,`dis_id`, `name`, `addres`, `cont`, `reqs`, `amt`,`date`) VALUES (null,'"+ctid1+"','"+username+"','"+adr+"','"+cnt+"','"+req+"','"+am+"','"+dtee+"')";
             System.out.println("sql==="+sql);
            try {
                statement = DdConnect.connect();
                isInserted = statement.executeUpdate(sql);
                if (isInserted > 0) {
                    out.print("1");
                    System.out.println("success");
                } else {
                    out.print("0");
                }
            } catch (Exception ex) {
                System.out.println(" Error In Register : " + ex);
                out.print("0");
            } finally {
                DdConnect.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
