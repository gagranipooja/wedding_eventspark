
<%@include  file="header.jsp"%>
<%@include  file="navigation.jsp"%>
<%@include  file="flash.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <div class="row background space20">
                <!--             <div class="span6">-->
                <form class="form-signin" action="regi_decorator" method="post" >
                    <center>         <h1><u>Decorator Registration</u></h1>
                        <table>
                            <tr>
                                <td><b>Username:</b></td>
                                <td><input type="text" name="username" class="input-block-level validate[required,custom[onlyLetterSp]]" placeholder="Username"><br></td>
                            </tr>
                            <tr>
                                <td><b>Password:</b></td>
                                <td><input type="password" name="password" class="input-block-level validate[required]" placeholder="Password"></td><br>
                            </tr>
<!--                            <tr>
                                <td><b>Re-enter Password:</b></td>
                                <td><input type="password" name="repassword" class="input-block-level validate[required,equals[Matchpass]]"></td><br>
                            </tr>-->
                            <tr>
                                <td><b>Email:</b></td>
                                <td><input type="text" name="email" class="input-block-level validate[required,custom[email]]" placeholder="Email"></td><br>
                            </tr>
                            <tr>
                                <td><b>Address</b></td>
                                <td><input type="text" name="address" class="input-block-level validate[required]" placeholder="Address"></td><br>
                            </tr>
                            <tr>
                                <td><b>Mobile No:</b></td>
                                <td><input type="number" name="mobile" class="input-block-level validate[required,custom[phone], minSize[10],maxSize[10]" placeholder="Mobile No"></td><br>
                            </tr>


                        </table>
                        <br>

                        <button type="submit" name="submit" class="btn btn-info">Submit</button>
                    </center>

                </form> 
                <!--             </div>-->
            </div>          
        </div>
    </body>
</html>
<%@include  file="footer.jsp"%>