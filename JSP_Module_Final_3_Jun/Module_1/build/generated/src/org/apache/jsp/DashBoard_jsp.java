package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.HashMap;

public final class DashBoard_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(5);
    _jspx_dependants.add("/header.jsp");
    _jspx_dependants.add("/navigation.jsp");
    _jspx_dependants.add("/manage_nav.jsp");
    _jspx_dependants.add("/flash.jsp");
    _jspx_dependants.add("/footer.jsp");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write('\n');
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Demo1</title>\n");
      out.write("        <link href=\"css/bootstrap.css\" rel=\"stylesheet\">\n");
      out.write("        <link href=\"css/font-awesome.css\" rel=\"stylesheet\">\n");
      out.write("        <link href=\"css/validationEngine.css\" rel=\"stylesheet\">\n");
      out.write("        <link href=\"css/style.css\" rel=\"stylesheet\">\n");
      out.write("        <link href=\"css/background.css\" rel=\"stylesheet\">\n");
      out.write("        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->\n");
      out.write("        <!--[if lt IE 9]>\n");
      out.write("          <script src=\"js/html5shiv.js\"></script>\n");
      out.write("          <script src=\"js/respond.min.js\"></script>\n");
      out.write("        <![endif]-->\n");
      out.write("        <script src=\"js/jquery-1.7.1.min.js\"></script>\n");
      out.write("        <script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("        <script src=\"js/jquery.validationEngine-en.js\"></script>\n");
      out.write("        <script src=\"js/jquery.validationEngine.js\"></script>\n");
      out.write("        <script src=\"js/highcharts.js\"></script>\n");
      out.write("        <script src=\"js/data.js\"></script>\n");
      out.write("        <script src=\"js/script.js\"></script>\n");
      out.write("    </head>\n");
      out.write("    <body>");
      out.write('\n');
      out.write("\n");
      out.write("\n");
      out.write("<div class=\"navbar navbar-inverse navbar-fixed-top\">\n");
      out.write("    <div class=\"navbar-inner\">\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->\n");
      out.write("            <button type=\"button\" class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\">\n");
      out.write("                <span class=\"icon-bar\"></span>\n");
      out.write("                <span class=\"icon-bar\"></span>\n");
      out.write("                <span class=\"icon-bar\"></span>\n");
      out.write("            </button>\n");
      out.write("            <a class=\"brand\" href=\"index.jsp\">Event Management </a>\n");
      out.write("            <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->\n");
      out.write("            <div class=\"nav-collapse collapse\">\n");
      out.write("                ");
String user = (String) session.getAttribute("userType");
                    String uname = (String) session.getAttribute("username");
                    if (user == null) {
      out.write("\n");
      out.write("                <ul class=\"nav\">\n");
      out.write("                    \n");
      out.write("                    ");
                        }
      out.write("\n");
      out.write("                    <!-- Read about Bootstrap dropdowns at http://twbs.github.com/bootstrap/javascript.html#dropdowns -->\n");
      out.write("                </ul>\n");
      out.write("                ");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<ul class=\"pull-right nav\">\n");
      out.write("    ");

        String userType = (String) session.getAttribute("userType");
        String username = (String) session.getAttribute("username");
        System.out.println("userType: " + userType);
        if (userType == null) {
    
      out.write("\n");
      out.write("    \n");
      out.write("    \n");
      out.write("    <!--<li><a href=\"user.jsp\">User Login</a></li>-->\n");
      out.write("    <li class=\"dropdown\">\n");
      out.write("        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Decorator <b class=\"caret\"></b></a>\n");
      out.write("        <ul class=\"dropdown-menu\">\n");
      out.write("            <li><a href=\"decorat_L.jsp\">Login</a></li>\n");
      out.write("            <li><a href=\"t_register.jsp\">Registration</a></li>\n");
      out.write("        </ul>\n");
      out.write("    </li>\n");
      out.write("    <li class=\"dropdown\">\n");
      out.write("        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Parlor<b class=\"caret\"></b></a>\n");
      out.write("        <ul class=\"dropdown-menu\">\n");
      out.write("            <li><a href=\"jockey.jsp\">Login</a></li>\n");
      out.write("            <li><a href=\"j_register.jsp\">Registration</a></li>\n");
      out.write("        </ul>\n");
      out.write("    </li>\n");
      out.write("    <li class=\"dropdown\">\n");
      out.write("        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Venue <b class=\"caret\"></b></a>\n");
      out.write("        <ul class=\"dropdown-menu\">\n");
      out.write("            <li><a href=\"jockey.jsp\">Login</a></li>\n");
      out.write("            <li><a href=\"j_register.jsp\">Registration</a></li>\n");
      out.write("        </ul>\n");
      out.write("    </li>\n");
      out.write("    <li class=\"dropdown\">\n");
      out.write("        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Catering <b class=\"caret\"></b></a>\n");
      out.write("        <ul class=\"dropdown-menu\">\n");
      out.write("            <li><a href=\"jockey.jsp\">Login</a></li>\n");
      out.write("            <li><a href=\"j_register.jsp\">Registration</a></li>\n");
      out.write("        </ul>\n");
      out.write("    </li>\n");
      out.write("<!--    <li><a href=\"logout.jsp\">Logout</a></li>\n");
      out.write("    ");
 } else {
//        System.out.println("in else");
        //for admin
        if (userType.equals("User")) {
      out.write("\n");
      out.write("          <li><a href=\"forimage.jsp\">image</a></li>\n");
      out.write("            <li><a href=\"new.jsp\">Example</a></li>\n");
      out.write("    <li><a href=\"predict.jsp\">Prediction</a></li>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("  \n");
      out.write("    <li><a href=\"owner.jsp\">Owner</a></li>\n");
      out.write("    <li class=\"dropdown\">\n");
      out.write("        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Owner <b class=\"caret\"></b></a>\n");
      out.write("        <ul class=\"dropdown-menu\">\n");
      out.write("            <li><a href=\"add_owner.jsp\"> Add </a></li>\n");
      out.write("            <li><a href=\"edit_owner.jsp\">Edit </a></li>\n");
      out.write("            <li><a href=\"delete_owner.jsp\">Delete</a></li>\n");
      out.write("        </ul>\n");
      out.write("    </li>\n");
      out.write("    <li class=\"dropdown\">\n");
      out.write("        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">User <b class=\"caret\"></b></a>\n");
      out.write("        <ul class=\"dropdown-menu\">\n");
      out.write("            <li><a href=\"add_user.jsp\"> Add </a></li>\n");
      out.write("            <li><a href=\"edit_user.jsp\">Edit </a></li>\n");
      out.write("            <li><a href=\"delete_user.jsp\">Delete</a></li>\n");
      out.write("        </ul>\n");
      out.write("    </li>\n");
      out.write("     <li><a href=\"add_race.jsp\">Add Race</a></li>\n");
      out.write("     <li><a href=\"view_races.jsp\">View Races</a></li>\n");
      out.write("     <li><a href=\"result.jsp\">Result</a></li>\n");
      out.write("  \n");
      out.write("    <li class=\"dropdown\">\n");
      out.write("        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Horse <b class=\"caret\"></b></a>\n");
      out.write("        <ul class=\"dropdown-menu\">\n");
      out.write("            <li><a href=\"add_horse.jsp\"> Add </a></li>\n");
      out.write("            <li><a href=\"allocate_horse.jsp\">Allocate </a></li>\n");
      out.write("            <li><a href=\"delete_horse.jsp\">Delete</a></li>\n");
      out.write("        </ul>\n");
      out.write("    </li>-->\n");
      out.write("  ");
}
      out.write("\n");
      out.write("    <li><a href=\"logout.jsp\">Logout</a></li>\n");
      out.write("    ");
 }
      out.write("\n");
      out.write("</ul>\n");
      out.write("\n");
      out.write("            </div><!--/.nav-collapse -->\n");
      out.write("        </div><!-- /.navbar-inner -->\n");
      out.write("    </div><!-- /.navbar -->\n");
      out.write("\n");
      out.write("</div> <!-- /.container -->\n");
      out.write('\n');

    //checking if flash message is set
    String flash_message = (String) session.getAttribute("flash_message");
    if (flash_message != null) {
        String flash_type = (String) session.getAttribute("flash_type");
        if (flash_type == null) {
            flash_type = "success";
        }
        session.removeAttribute("flash_message");
        session.removeAttribute("flash_type");

      out.write("\n");
      out.write("<div class=\"container\">\n");
      out.write("    <div class=\"alert alert-");
      out.print(flash_type);
      out.write("\">\n");
      out.write("        <button class=\"close\" data-dismiss=\"alert\"></button>\n");
      out.write("        <strong>");
      out.print(flash_type);
      out.write("!</strong> ");
      out.print(flash_message);
      out.write("\n");
      out.write("    </div>\n");
      out.write("</div>\n");

    }

      out.write("\n");
      out.write("        \n");
      out.write("        <h1>Home</h1>\n");
      out.write("<hr class=\"featurette-divider\">\n");
      out.write("<footer>\n");
      out.write("    <div class=\"container\">\n");
      out.write("        <div class=\"muted credit\">\n");
      out.write("            <p class=\"pull-right\"><a href=\"#\">Back to top</a></p>\n");
      out.write("            <p>&copy; 2015 Company, Inc. &middot; <a href=\"#\">Privacy</a> &middot; <a href=\"#\">Terms</a></p>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</footer>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
